# Contao-Button

## Function & usage

This extension for Contao Open Source CMS allows you to create a button as content element. You can select between multiple button styles, add additional classes and display the button in full width and also in an inverted appearance. Before use, the options for the button styles must be configured within the `config.yml`.

## Configuration

### Adjust button styles

The default configuration of the options for the button style is empty. You have to define it by adjusting the `config.yml` file.

```yml
contao_button:
  button_styles:
    - foo
    - bar
```
