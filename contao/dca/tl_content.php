<?php

declare(strict_types=1);

$GLOBALS['TL_DCA']['tl_content']['palettes']['button'] =
    '{type_legend},type,headline;'
    . '{link_legend},url,target,linkTitle,titleText;'
    . '{button_legend},buttonStyle,buttonClass,fullWidth,invertStyle;'
    . '{template_legend:hide},customTpl;'
    . '{protected_legend:hide},protected;'
    . '{expert_legend:hide},guests,cssID;'
    . '{invisible_legend:hide},invisible,start,stop'
;

$GLOBALS['TL_DCA']['tl_content']['fields']['buttonStyle'] = [
    'inputType' => 'select',
    'default' => '',
    'eval' => array('tl_class'=>'w50', 'includeBlankOption'=>true),
    'sql' => "varchar(32) NOT NULL default"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['buttonClass'] = [
    'inputType' => 'text',
    'eval' => array('tl_class'=>'w50'),
    'sql'  => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['fullWidth'] = [
    'inputType' => 'checkbox',
    'eval' => array('tl_class'=>'w50'),
    'sql' => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['invertStyle'] = [
    'inputType' => 'checkbox',
    'eval' => array('tl_class'=>'w50'),
    'sql' => "char(1) NOT NULL default ''"
];
