<?php

declare(strict_types=1);

namespace designerei\ContaoButtonBundle\Controller\ContentElement;

use Contao\ContentModel;
use Contao\CoreBundle\Controller\ContentElement\AbstractContentElementController;
use Contao\CoreBundle\DependencyInjection\Attribute\AsContentElement;
use Contao\CoreBundle\Twig\FragmentTemplate;
use Contao\CoreBundle\InsertTag\InsertTagParser;
use Contao\CoreBundle\String\HtmlAttributes;
use Contao\Validator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

#[AsContentElement(category: 'links', template: 'content_element/button')]
class ButtonController extends AbstractContentElementController
{
    private $insertTagParser;

    public function __construct(InsertTagParser $insertTagParser)
    {
        $this->insertTagParser = $insertTagParser;
    }

    protected function getResponse(FragmentTemplate $template, ContentModel $model, Request $request): Response
    {
        // Link with attributes
        $href = $this->insertTagParser->replaceInline($model->url ?? '');

        if (Validator::isRelativeUrl($href)) {
            $href = $request->getBasePath().'/'.$href;
        }

        $linkAttributes = (new HtmlAttributes())
            ->set('href', $href)
            ->setIfExists('title', $model->linkTitle)
            ->setIfExists('data-lightbox', $model->rel)
        ;

        if ($model->target) {
            $linkAttributes
                ->set('target', '_blank')
                ->set('rel', 'noreferrer noopener')
            ;
        }

        // Button
        if ($model->buttonStyle) {
            $linkAttributes
                ->addClass($model->buttonStyle)
            ;
        } else {
            $linkAttributes
                ->addClass('btn')
            ;
        }

        if ($model->buttonClass) {
            $linkAttributes
                ->addClass($model->buttonClass)
            ;
        }

        if ($model->fullWidth) {
            $linkAttributes
                ->addClass('full')
            ;
        }

        if ($model->invertStyle) {
            $linkAttributes
                ->addClass('inverted')
            ;
        }

        $template->set('href', $href);
        $template->set('link_attributes', $linkAttributes);

        // Link text
        $template->set('link_text', $model->linkTitle ?: $href);

        return $template->getResponse();
    }
}