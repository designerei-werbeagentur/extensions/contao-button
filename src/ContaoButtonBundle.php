<?php

declare(strict_types=1);

namespace designerei\ContaoButtonBundle;

use designerei\ContaoButtonBundle\DependencyInjection\ContaoButtonExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContaoButtonBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }

    public function getContainerExtension(): ContaoButtonExtension
    {
        return new ContaoButtonExtension();
    }
}
