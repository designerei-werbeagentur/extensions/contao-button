<?php

declare(strict_types=1);

namespace designerei\ContaoButtonBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('contao_button');

        // Keep compatibility with symfony/config < 4.2
        if (method_exists($treeBuilder, 'getRootNode')) {
           $rootNode = $treeBuilder->getRootNode();
        } else {
           $rootNode = $treeBuilder->root('contao_button');
        }

        $rootNode
            ->children()
                ->arrayNode('button_styles')
                    ->scalarPrototype()->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
