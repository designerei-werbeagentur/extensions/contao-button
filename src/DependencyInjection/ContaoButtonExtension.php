<?php

declare(strict_types=1);

namespace designerei\ContaoButtonBundle\DependencyInjection;

use designerei\ContaoButtonBundle\EventListener\ButtonStylesOptionsListener;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Definition;


class ContaoButtonExtension extends Extension
{
    public function getAlias(): string
    {
        return 'contao_button';
    }

    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../../config'));
        $loader->load('services.yml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $definition = $container->getDefinition(ButtonStylesOptionsListener::class);
        $definition->setArgument(0, $config['button_styles']);
    }
}
