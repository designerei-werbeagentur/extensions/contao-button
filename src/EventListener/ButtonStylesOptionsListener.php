<?php

declare(strict_types=1);

namespace designerei\ContaoButtonBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Callback;

/**
 * @Callback(table="tl_content", target="fields.buttonStyle.options")
 */
class ButtonStylesOptionsListener
{
    private array $config;

    public function __construct(array $config)
     {
       $this->buttonStyles = $config;
     }

     public function __invoke(): array {
       return $this->buttonStyles;
     }
}
